package it.com.burningcode.jira.plugin;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.testkit.client.util.TimeBombLicence;

public class IntegrationTestWatcherFieldSettings extends FuncTestCase {

	public IntegrationTestWatcherFieldSettings() {
		// TODO Auto-generated constructor stub
	}

    @Override
    protected void setUpTest() {
        backdoor.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
        super.setUpTest();
    }

    /**
	 * Test to verify Issue #11 is fixed
	 */
	public void testSettingsLink() {
		navigation.gotoAdmin();
		navigation.clickLinkWithExactText("Add-ons");
		assertions.getLinkAssertions().assertLinkWithExactTextAndUrlPresent("Watcher Field Settings", "/secure/project/WatcherFieldSettings.jspa");
	}
    
    public void testChangeSettings() {
    	// Browse to the settings page
    	navigation.gotoResource("WatcherFieldSettings.jspa");
    	
    	// Click on the edit buttong
    	tester.clickLink("edit_watcher_field_settings");
    	
    	// 
        tester.assertRadioOptionSelected("ignorePermissions", "false");
        tester.setFormElement("ignorePermissions", "true");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
        tester.submit("Update");
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
    }

    public void testAccessWithoutPermissions() {
        navigation.login(FRED_USERNAME);
        navigation.gotoResource("WatcherFieldSettings.jspa");
        assertions.getTextAssertions().assertTextPresent("Access Denied");
    }
}
