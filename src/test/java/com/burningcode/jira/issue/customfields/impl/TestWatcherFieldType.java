package com.burningcode.jira.issue.customfields.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.burningcode.jira.plugin.WatcherFieldSettings;
import com.opensymphony.module.propertyset.PropertySet;
import org.junit.runner.RunWith;
import org.mockito.internal.stubbing.answers.DoesNothing;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.OngoingStubbing;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.converters.MultiUserConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.opensymphony.module.propertyset.PropertySetManager;
import com.opensymphony.module.propertyset.map.MapPropertySet;

import junit.framework.TestCase;

import webwork.action.ActionContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PropertySetManager.class, ActionContext.class, ComponentAccessor.class, WatcherFieldSettings.class})
public class TestWatcherFieldType extends TestCase {
	private CustomFieldParams customFieldParams;
	private Project project;
	private PermissionManager permissionManager;
    private GlobalPermissionManager globalPermissionManager;
	private JiraAuthenticationContext authenticationContext;
	private WatcherFieldType watcherFieldType;
	private WatcherManager watcherManager;
	private UserUtil userUtil;

	private JiraAuthenticationContext getAuthenticationContext(){
		return mock(JiraAuthenticationContext.class);
	}
	
	private CustomFieldParams getCustomFieldParams(){
		return mock(CustomFieldParams.class);
	}
	
	private FieldConfig getFieldConfig(){
		FieldConfig config = mock(FieldConfig.class);
		when(config.getFieldId()).thenReturn("customField_10000");
		return config;
	}
	
	private Issue getIssue(){
		return getIssue(true, this.project);
	}

	private Issue getIssue(boolean isCreated, Project project){
		Issue issue = mock(Issue.class);
		when(issue.isCreated()).thenReturn(isCreated);
		when(issue.getProjectObject()).thenReturn(project);
		return issue;
	}
	
	private IssueManager getIssueManager(){
		PowerMockito.mockStatic(ComponentAccessor.class);
		IssueManager issueManager = mock(IssueManager.class);
		when(ComponentAccessor.getIssueManager()).thenReturn(issueManager);
		return issueManager;
	}

	private PermissionManager getPermissionManager(){
		return getPermissionManager(true);
	}

	private PermissionManager getPermissionManager(boolean hasPermission){
		PermissionManager permissionManager = mock(PermissionManager.class);
		when(permissionManager.hasPermission(eq(ProjectPermissions.BROWSE_PROJECTS), (Project)anyObject(), (ApplicationUser)anyObject())).thenReturn(hasPermission);

		return permissionManager;
	}

    private GlobalPermissionManager getGlobalPermissionManager(){
        return getGlobalPermissionManager(true);
    }

    private GlobalPermissionManager getGlobalPermissionManager(boolean hasPermission){
        GlobalPermissionManager globalPermissionManager = mock(GlobalPermissionManager.class);
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), (ApplicationUser) anyObject())).thenReturn(hasPermission);

        return globalPermissionManager;
    }
	
	private Project getProject(){
		Project project = mock(Project.class);
		when(project.getId()).thenReturn((long) 10000);

		return project;
	}
	
	private ProjectManager getProjectManager(){
		PowerMockito.mockStatic(ComponentAccessor.class);
		ProjectManager projectManager = mock(ProjectManager.class);
		when(ComponentAccessor.getProjectManager()).thenReturn(projectManager);
		when(projectManager.getProjectObj(project.getId())).thenReturn(project);
		
		return projectManager;
	}
	
	private void setupActionContext(Map<Object, String> params){
		when(ActionContext.getParameters()).thenReturn(params);
	}
	
	private void setupPropertySetManager(){
		PowerMockito.mockStatic(PropertySetManager.class);
		MapPropertySet propertySet = new MapPropertySet();
		propertySet.setMap(new HashMap<String, Object>());
		when(PropertySetManager.getInstance(eq("ofbiz"), anyMap())).thenReturn(propertySet);
	}
	
	private ApplicationUser getUser(String name){
		ApplicationUser user = mock(ApplicationUser.class);
		when(user.getName()).thenReturn(name);
		when(user.toString()).thenReturn(name);
		return user;
	}
	
	/**
	 * Gets a list of 10 User objects for testing with {@code TestWatcherFieldType#watcherManager}.
	 * 
	 * @return List of 10 User objects.
	 */
	private ArrayList<ApplicationUser> getUserList(){
		return getUserList(10, this.watcherManager);
	}
	
	/**
	 * Get a list of User objects for testing.  The first and last objects in the list are
	 * set to already be watching when the {@code WatcherManager#isWatching(User, GenericValue)} is called.
	 * 
	 * @param count The number of Users to have in the list
	 * @param watcherManager The WatcherManager to use to call isWatching.
	 * @return List of User objects
	 */
	private ArrayList<ApplicationUser> getUserList(int count, WatcherManager watcherManager){
		ArrayList<ApplicationUser> users = new ArrayList<ApplicationUser>();
		for(int i = 0; i < count; i++){
			ApplicationUser user = mock(ApplicationUser.class);
			
			// Make the first and last User objects watching the issue
			if(i == 0 || i == (count - 1)){
				when(watcherManager.isWatching(eq(user), (Issue)anyObject())).thenReturn(true);
			}else{
				when(watcherManager.isWatching(eq(user), (Issue)anyObject())).thenReturn(false);
			}
			users.add(user);
		}
		return users;
	}
	
	private UserUtil getUserUtil(){
		UserUtil userUtil = mock(UserUtil.class);
		when(userUtil.getUserByName(anyString())).thenAnswer(new Answer<ApplicationUser>() {

			public ApplicationUser answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				ApplicationUser user = mock(ApplicationUser.class);
				when(user.getName()).thenReturn((String)args[0]);

				return user;
			}
		});

		return userUtil;
	}

	private WatcherFieldType getWatcherFieldType(){
		return getWatcherFieldType(
			authenticationContext,
			permissionManager,
            globalPermissionManager,
			watcherManager,
			userUtil);
	}
	private WatcherFieldType getWatcherFieldType(
			JiraAuthenticationContext authenticationContext, 
			PermissionManager permissionManager,
            GlobalPermissionManager globalPermissionManager,
			WatcherManager watcherManager,
			UserUtil userUtil){

		WatcherFieldType watcherFieldType = new WatcherFieldType(
			mock(CustomFieldValuePersister.class),
			mock(GenericConfigManager.class),
			mock(MultiUserConverter.class),
			mock(ApplicationProperties.class),
			mock(JiraAuthenticationContext.class),
			mock(UserPickerSearchService.class),
			mock(FieldVisibilityManager.class),
			mock(JiraBaseUrls.class),
            mock(UserBeanFactory.class),
			permissionManager,
            globalPermissionManager,
			watcherManager,
			userUtil
		);

		return watcherFieldType;
	}
	
	private WatcherManager getWatcherManager(){
		WatcherManager manager = mock(WatcherManager.class);
		when(manager.isWatchingEnabled()).thenReturn(true);
		return manager;
	}

	@Override
	protected void setUp() throws Exception {
		customFieldParams = getCustomFieldParams();
		project = getProject();
		permissionManager = getPermissionManager();
        globalPermissionManager = getGlobalPermissionManager();
		authenticationContext = getAuthenticationContext();
		watcherFieldType = getWatcherFieldType();
		watcherManager = getWatcherManager();
		userUtil = getUserUtil();
		
		setupPropertySetManager();
		
		super.setUp();
	}

	/**
	 * Tests {@link WatcherFieldType#addWatchers(com.atlassian.jira.issue.Issue, java.util.Collection)} passing a list of usernames (Strings)
	 */
	public void testAddWatchersWithUsernames(){
        Issue issue = getIssue();
        WatcherFieldType fieldType = getWatcherFieldType();

        when(fieldType.isIssueEditable(issue)).thenReturn(true);
        when(watcherManager.isWatching((ApplicationUser)anyObject(), eq(issue))).thenReturn(true);

        ArrayList<String> usernames = new ArrayList<String>();
        usernames.add("bob");
        usernames.add("jim");

        final ArrayList<ApplicationUser> watchedUsers = new ArrayList<ApplicationUser>();
        for(String username : usernames) {
            watchedUsers.add(userUtil.getUserByName(username));
        }

        whenStartWatching(watcherManager, watchedUsers);

		fieldType.addWatchers(issue, usernames);

		assertEquals("Expected watched users is different than actual", usernames.size(), watchedUsers.size());

		for(ApplicationUser watchedUser : watchedUsers){
			for(int i = 0; i < usernames.size(); i++){
				assertTrue("User not set as watcher that should", usernames.contains(watchedUser.getName()));
			}
		}
	}
	
	/**
	 * Tests {@link WatcherFieldType#addWatchers(com.atlassian.jira.issue.Issue, java.util.Collection)} passing a list of User objects
	 */
	public void testAddWatchersWithUserObjects(){
		ArrayList<ApplicationUser> users = getUserList();
		
		// Get the users added as watchers for checking
		final ArrayList<ApplicationUser> watchedUsers = new ArrayList<ApplicationUser>();
		whenStartWatching(watcherManager, watchedUsers);

        Issue issue = getIssue();
		WatcherFieldType fieldType = getWatcherFieldType();
        when(fieldType.isIssueEditable(issue)).thenReturn(true);
		fieldType.addWatchers(issue, users);
		assertEquals("Incorrect number of users watching issue", 8, watchedUsers.size());
		
		for(int i = 0; i < users.size(); i++){
			if(i == 0 || i == 9){
				assertFalse("User set as watcher that shouldn't", watchedUsers.contains(users.get(i)));
			}else{
				assertTrue("User not set as watcher that should", watchedUsers.contains(users.get(i)));
			}
		}
	}
	
	/**
	 * Tests that {@link WatcherFieldType#getChangelogValue(CustomField, java.util.Collection)} works correctly.
	 * 
	 * Added test for JWFP-28 fix
	 */
	public void testGetChangelogValue() {
		ApplicationUser watcher01 = mock(ApplicationUser.class);
		when(watcher01.getDisplayName()).thenReturn("User Watcher 01");
		ApplicationUser watcher02 = mock(ApplicationUser.class);
		when(watcher02.getDisplayName()).thenReturn(null);
		when(watcher02.getName()).thenReturn("Watcher02");
		ApplicationUser watcher03 = mock(ApplicationUser.class);
		when(watcher03.getDisplayName()).thenReturn("User Watcher 03");

		ArrayList<ApplicationUser> watchers = new ArrayList<ApplicationUser>();
		watchers.add(watcher01);
		watchers.add(watcher02);
		watchers.add(null);
		watchers.add(watcher03);

		WatcherFieldType fieldType = getWatcherFieldType();
		String expected = "User Watcher 01, Watcher02, User Watcher 03";
		String actual = fieldType.getChangelogValue(mock(CustomField.class), watchers);
		assertEquals(expected, actual);
	}
	
	public void testIsIssueEditable(){
		WatcherFieldType fieldType = getWatcherFieldType();
        Issue issue = getIssue();

        when(fieldType.isIssueEditable(issue)).thenReturn(true);
		
		// Verify returns true when issue is fully editable
		assertTrue("Issue is not editable when it should be.", fieldType.isIssueEditable(issue));

        when(fieldType.isIssueEditable(issue)).thenReturn(false);

		// Verify returns false when the user doesn't have permission to edit issue.
		assertFalse("Issue is editable when it shouldn't be.", fieldType.isIssueEditable(issue));

        issue = getIssue(false, project);
        when(fieldType.isIssueEditable(issue)).thenReturn(true);

		// Verify returns false when issue is not created
		assertFalse("Issue is editable when it shouldn't be.", fieldType.isIssueEditable(issue));
	}

    public void testIsUserPermitted() {
        PowerMockito.mockStatic(WatcherFieldSettings.class);

        WatcherFieldType fieldType = getWatcherFieldType();
        Issue issue = getIssue();
        ApplicationUser user = getUser("bob");
        PropertySet propertySet = mock(PropertySet.class);

        when(authenticationContext.getUser()).thenReturn(user);
        when(propertySet.exists(eq("ignorePermissions"))).thenReturn(true);
        when(propertySet.getBoolean(eq("ignorePermissions"))).thenReturn(true);
        when(WatcherFieldSettings.getPropertySet()).thenReturn(propertySet);
        when(permissionManager.hasPermission(ProjectPermissions.MANAGE_WATCHERS, issue.getProjectObject(), user)).thenReturn(true);

        assertTrue("User does not have permissions when it should.", fieldType.isIssueEditable(issue));
    }

    public void testIsUserPermittedWithUserNull() {
        PowerMockito.mockStatic(WatcherFieldSettings.class);

        WatcherFieldType fieldType = getWatcherFieldType();
        Issue issue = getIssue();
        PropertySet propertySet = mock(PropertySet.class);

        when(authenticationContext.getUser()).thenReturn(null);
        when(propertySet.exists(eq("ignorePermissions"))).thenReturn(true);
        when(propertySet.getBoolean(eq("ignorePermissions"))).thenReturn(true);
        when(WatcherFieldSettings.getPropertySet()).thenReturn(propertySet);

        assertTrue("User does not have permissions when it should.", fieldType.isIssueEditable(issue));
    }

    public void testIsUserPermittedAsWatcherForProject() {
        WatcherFieldType fieldType = getWatcherFieldType();

        ApplicationUser user = getUser("jim");
        Project project = getProject();
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, project, user)).thenReturn(true);

        assertTrue("User is not permitted as watcher for the project when it should.", fieldType.isUserPermittedAsWatcher(user, project));
        assertFalse("User is permitted as watcher for the project when it should not be.", fieldType.isUserPermittedAsWatcher(user, (Project)null));

        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, project, user)).thenReturn(false);

        // Test when user is not permitted
        assertFalse("User is permitted as watcher for the project when it should not be.", fieldType.isUserPermittedAsWatcher(user, project));
    }

    public void testIsUserPermittedAsWatcherForIssue() {
        WatcherFieldType fieldType = getWatcherFieldType();

        ApplicationUser user = getUser("jim");
        Issue issue = getIssue();
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(true);

        assertTrue("User is not permitted as watcher for the issue when it should.", fieldType.isUserPermittedAsWatcher(user, issue));
        assertTrue("When issue is null, user does not have permissions for the issue when it should.", fieldType.isUserPermittedAsWatcher(user, (Issue)null));

        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)).thenReturn(false);

        // Test when user is not permitted
        assertFalse("User is permitted as watcher for the issue when it should not be.", fieldType.isUserPermittedAsWatcher(user, issue));
    }

	/**
	 * TODO: Make unit test work
	 */
	public void testValidateFromParams(){
		final ArrayList<ApplicationUser> watchers = new ArrayList<ApplicationUser>();
		
		ApplicationUser admin = getUser("admin");
		watchers.add(admin);

		ApplicationUser bob = getUser("bob");
		watchers.add(bob);

		whenValueFromCustomFieldParams().thenReturn(watchers);
		
		final ArrayList<HashMap<String, Object>> errors = new ArrayList<HashMap<String, Object>>();
		ErrorCollection errorCollectionToAddTo = mock(ErrorCollection.class);

		doAnswer(new DoesNothing() {
			public Object answer(InvocationOnMock invocation) {
	            Object[] args = invocation.getArguments();
	            HashMap<String, Object> error = new HashMap<String, Object>();
	            error.put("field", args[0]);
	            error.put("error", args[1]);
	            error.put("reason", args[2]);
	            errors.add(error);
	            return null;
	           }
		}).when(errorCollectionToAddTo).addError(anyString(), anyString(), any(ErrorCollection.Reason.class));

		PowerMockito.mockStatic(ActionContext.class);
		HashMap<String, String[]> params = new HashMap<String, String[]>();
		when(ActionContext.getParameters()).thenReturn(params);
		
		params.put("pid", new String[]{"10000"});
		
//		watcherFieldType.validateFromParams(customFieldParams, errorCollectionToAddTo, fieldConfig);
		//params.put("id", null);

		
//		Issue issue = getIssue();
//		when(issueManager.getIssueObject(arg0))
		
//		fieldType.validateFromParams(relevantParams, errorCollectionToAddTo, config);
//		
//		
//		//validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config)
	}
	
	private <T> OngoingStubbing<Collection<ApplicationUser>> whenValueFromCustomFieldParams(){
		return when(watcherFieldType.getValueFromCustomFieldParams(customFieldParams));
	}
	
	/**
	 * Sets {@code WatcherManager#startWatching(User, GenericValue)} to add users to the
	 * watchedUsers list.
	 * 
	 * @param watcherManager The WatcherManager with {@code WatcherManager#startWatching(User, GenericValue)} method being called 
	 * @param watchedUsers The list that will store the watching User objects.
	 */
	private void whenStartWatching(WatcherManager watcherManager, final ArrayList<ApplicationUser> watchedUsers){
		doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				watchedUsers.add((ApplicationUser)args[0]);
				return null;
			}
		}).when(watcherManager).startWatching((ApplicationUser)anyObject(), (Issue)anyObject());
	}
}
